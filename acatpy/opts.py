# -*- coding: utf-8 -*-

from optparse import OptionParser

from acatpy.io.speak import error

def make_opt(*args, **kw):
    class P(object): pass
    o = P()
    o.args = args
    o.kw = kw
    return o

def get_opts(usage, opts, help_extra=''):
    orig_help = None

    def err(parser, **kw):
        def inner(*x):
            error('%s\n' % ' '.join(x))
            parser.print_help()
            exit(1)
        return inner

    def print_help(*k, **kw):
        orig_help(*k, **kw)
        if help_extra:
            print('\n' + help_extra)

    parser = OptionParser(usage)
    orig_help = parser.print_help
    parser.error = err(parser)
    parser.print_help = print_help
    for opt in opts:
        parser.add_option(*opt.args, **opt.kw)
    options, args = parser.parse_args()
    return options, args, parser
