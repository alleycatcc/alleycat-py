from os import isatty
import re

def color(n, s):
    if isatty(1): return '\x1b[%sm%s\x1b[0m' % (n, s)
    else: return s

def underline(s): return color(4, s)

def red(s): return color(31, s)

def green(s): return color(32, s)

def yellow(s): return color(33, s)

def blue(s): return color(34, s)

def magenta(s): return color(35, s)

def cyan(s): return color(36, s)

def bright_red(s): return color(91, s)

def bright_green(s): return color(92, s)

def bright_yellow(s): return color(93, s)

def bright_blue(s): return color(94, s)

def bright_magenta(s): return color(95, s)

def bright_cyan(s): return color(96, s)

def strip_colors(x):
    return re.sub('\x1b\\[\\d+?m', '', x)
