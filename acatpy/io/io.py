# -*- coding: utf-8 -*-

import os
import os.path as path
from shutil import rmtree
from signal import Signals
import subprocess
from subprocess import PIPE
from sys import exit, argv

from acatpy.colors import bright_red, yellow
from acatpy.io.speak import error, info_cmd, info, warn
from acatpy.speak import shell_quote
from acatpy.util import alias_kw, imerge

signals = {_.value: _.name for _ in Signals}

class CmdReturn(object):
    def __init__(self, ok=None, ret=None, errstr=None):
        self.ok = ok
        self.out = ret.stdout if ret else None
        self.err = ret.stderr if ret else None
        self.errstr = errstr
        self.ret = ret

def die(s=None, **kw):
    if s: error(s)
    else: error()
    code = kw.get('code')
    if code is None: code = 1
    exit(code)

def dirscript():
    return path.dirname(path.realpath(argv[0]))

def _ret_streams_decode(ret, decode_stdout, decode_stderr):
    se = ret.stderr
    so = ret.stdout

    def dec(x, do):
        if x is None: return None
        if not do: return x
        return x.decode()
    sed = dec(se, decode_stderr)
    sod = dec(so, decode_stdout)
    ret.stdout = sod
    ret.stderr = sed
    return ret

def signal_lookup(sig):
    return signals.get(sig) or 'unknown: %s' % sig

def mkdir(*args, **kw):
    return mkd(*args, **kw)

@alias_kw(diearg='die')
def mkd(the_path, only_leaf=False, diearg=True):
    if path.exists(the_path): return
    f = 'mkdir' if only_leaf else 'makedirs'
    try:
        getattr(os, f)(the_path)
    except OSError as e:
        errstr = "Couldn't mkdir %s: %s" % (bright_red(the_path), e)
        if diearg: die(errstr)
        return warn(errstr)
    return True

def chdir(*args, **kw):
    return chd(*args, **kw)

@alias_kw(diearg='die')
def chd(the_dir, diearg=True):
    info('[ %s ] %s' % (yellow('chdir'), the_dir))
    try:
        os.chdir(the_dir)
    except OSError as e:
        errstr = "Couldn't chdir to %s: %s" % (bright_red(the_dir), e)
        if diearg: die(errstr)
        return warn(errstr)
    return True

# --- @todo cmd, die
def cpa(src, tgt, **kw):
    if 'check' not in kw:
        kw['check'] = True
    info('[ %s ] %s -> %s' % (yellow('cpa'), shell_quote(src), shell_quote(tgt)))
    subprocess.run(['cp', '-a', src, tgt], **kw)

@alias_kw(diearg='die')
def rm(path, isdir=False, diearg=True, throw=False, verbose=True):
    if isdir:
        rmstr = 'rmdir'
    else:
        rmstr = 'rm'
    if verbose:
        info('[ %s ] %s' % (bright_red(rmstr), shell_quote(path)))
    if throw: diearg = False
    try:
        if isdir:
            def onerror(function, path, excinfo):
                _, exc, _ = excinfo
                errstr = "Couldn't rmdir %s: %s" % (bright_red(path), exc)
                if diearg: die(errstr)
                elif throw: raise Exception(errstr)
                warn(errstr)
            rmtree(path, ignore_errors=False, onerror=onerror)
        else:
            os.unlink(path)
    except OSError as e:
        errstr = "Couldn't remove %s: %s" % (bright_red(path), e)
        if diearg: die(errstr)
        elif throw: raise Exception(errstr)
        return warn(errstr)
    return True

def rmdir(*args, **kw):
    return rmd(*args, **kw)

def rmd(path, **kw):
    return rm(path, isdir=True, **kw)

@alias_kw(diearg='die')
def cmd(
        cmdspec, *args, pt=None, verbose=True,
        input=None,
        shell=None,
        cwd=None,
        capture_stdout=False, capture_stderr=False,
        decode_stdout=True, decode_stderr=True,
        complain=True,
        throw=False,
        diearg=False):
    pt = pt or {}
    if shell is not None: pt['shell'] = shell
    save_cwd = None
    if cwd:
        save_cwd = os.getcwd()
        if not chdir(cwd, die=diearg):
            warn("Couldn't chdir to %s" % bright_red(cwd))
            return CmdReturn(ok=False)
    cmdspec = [str(_) for _ in cmdspec]
    if verbose: info_cmd(cmdspec, shell=shell)
    cmd_kw = imerge({'check': throw}, pt)
    if capture_stderr: cmd_kw['stderr'] = PIPE
    if capture_stdout: cmd_kw['stdout'] = PIPE
    if input: cmd_kw['input'] = input
    cmd_sq = ' '.join(shell_quote(_) for _ in cmdspec)
    try:
        if shell: to_run = ' '.join(cmdspec)
        else: to_run = cmdspec
        ret = subprocess.run(to_run, *args, **cmd_kw)
    except FileNotFoundError:
        err_str = "Couldn't run cmd %s: file not found." % (bright_red(cmd_sq))
        if diearg: die(err_str)
        else:
            warn(err_str)
            if save_cwd:
                if not chdir(save_cwd, die=diearg):
                    warn("Couldn't restore chdir to %s" % bright_red(save_cwd))
                    return CmdReturn(ok=False)
            return CmdReturn(ok=False, errstr=err_str)
    ret = _ret_streams_decode(ret, decode_stdout, decode_stderr)
    rc = ret.returncode
    if rc == 0:
        if save_cwd:
            if not chdir(save_cwd, die=diearg):
                warn("Couldn't restore chdir to %s" % bright_red(save_cwd))
                return CmdReturn(ok=False, ret=ret)
        return CmdReturn(ok=True, ret=ret)
    sig = signal_lookup(-1 * rc) if rc < 0 else None
    sig_str = ' (terminated by signal %s)' % yellow(sig) if sig else ''
    se = ret.stderr
    so = ret.stdout
    se_str = ': %s' % se if se else '.'
    err_str = "Cmd unsuccessful %s%s%s" % (bright_red(cmd_sq), sig_str, se_str)
    if diearg:
        if so: info('stdout: %s' % so)
        die(err_str)
    else:
        if complain:
            warn(err_str)
    if save_cwd:
        if not chdir(save_cwd, die=diearg):
            warn("Couldn't restore chdir to %s" % bright_red(save_cwd))
            return CmdReturn(ok=False, ret=ret, errstr=err_str)
    return CmdReturn(ok=False, ret=ret, errstr=err_str)
