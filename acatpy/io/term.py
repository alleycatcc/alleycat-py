# -*- coding: utf-8 -*-

import signal
import sys
import termios
import tty

from acatpy.io.speak import sayn, say
from acatpy.util import deep_merge_dict

DEBUG_KEYS = False

key_table = {
    (1,): 'ctrl-a',
    (21,): 'ctrl-u',

    (27, 91, 65): 'arrow-up',
    (27, 91, 66): 'arrow-down',
    (27, 91, 67): 'arrow-right',
    (27, 91, 68): 'arrow-left',

    (27, 91, 49, 126): 'home',
    (27, 91, 52, 126): 'end',

    (27, 91, 72): 'home',
    (27, 91, 70): 'end',
    (27, 79, 72): 'home',
    (27, 79, 70): 'end',

    # --- rxvt
    (27, 91, 55, 126): 'home',
    (27, 91, 56, 126): 'end',

    (27, 91, 51, 126): 'delete',

    (13,): 'enter',

    (8,): 'backspace',  # used?
    (127,): 'backspace',
}

keymap = {'keymap': None}

def init():
    keymap['keymap'] = get_key_table(key_table)

def hide_cursor():
    sayn('[?25l')

def show_cursor():
    sayn('[?25h')

def _sigint(*args, term_restore=None):
    if term_restore is not None:
        term_restore()
    sys.exit(0)

def term_clear():
    sayn('[2J')

# ---
# Sets the terminal to raw, but optionally allows interrupt signals (Ctl-c /
# Ctl-z / etc.) to still work.
# In that case `term_cbreak` is probably more useful though.

def term_rawlike(do_sigint=True, sigint=_sigint, enable_interrupt_signals=None):
    hide_cursor()
    term_restore = term_setraw(enable_interrupt_signals=enable_interrupt_signals)

    def restore():
        term_restore()
        show_cursor()

    if do_sigint:
        callback = lambda *args: sigint(*args, term_restore=restore)
        signal.signal(signal.SIGINT, callback)
    return restore

def term_cbreak():
    save_attr = termios.tcgetattr(1)
    tty.setcbreak(1)
    return lambda: termios.tcsetattr(1, termios.TCSADRAIN, save_attr)

def term_setraw(enable_interrupt_signals=True):
    save_attr = termios.tcgetattr(1)
    tty.setraw(1)
    if enable_interrupt_signals:
        iflag, oflag, cflag, lflag, ispeed, ospeed, cc = termios.tcgetattr(1)
        # --- re-enable ctl c / ctl z
        lflag |= termios.ISIG
        # --- TCSANOW: immediate
        # --- TCSADRAIN: wait for output to flush
        termios.tcsetattr(1, termios.TCSANOW, [iflag, oflag, cflag, lflag, ispeed, ospeed, cc])
    return lambda: termios.tcsetattr(1, termios.TCSADRAIN, save_attr)

# --- There is quite some repetition in the sync and async versions of
# `getch_combo` and `getch_combo_async` but refactoring seems complicated.

async def _getch_combo_async(in_stream, the_keymap, combo=None):
    combo = combo or []
    ch = await in_stream.read(1)
    och = ord(ch)
    combo = [*combo, (ch, och)]
    cont = the_keymap.get(ord(ch))
    if not cont:
        return None, combo
    if isinstance(cont, dict):
        return await _getch_combo_async(in_stream, cont, combo=combo)
    name = cont
    return name, combo

async def getch_combo_async(in_stream):
    name, keys = await _getch_combo_async(in_stream, keymap['keymap'])
    if DEBUG_KEYS:
        say('got %s %s\n\n\n\n' % (name, '+'.join(str(x[1]) for x in keys)))
    return name, keys

def _getch_combo(the_keymap, combo=None):
    combo = combo or []
    ch = getch()
    och = ord(ch)
    combo = [*combo, (ch, och)]
    cont = the_keymap.get(ord(ch))
    if not cont:
        return None, combo
    if isinstance(cont, dict):
        return _getch_combo(cont, combo=combo)
    name = cont
    return name, combo

def getch():
    return sys.stdin.read(1)

def getch_combo():
    name, keys = _getch_combo(keymap['keymap'])
    if DEBUG_KEYS:
        say('got %s %s\n\n\n\n' % (name, '+'.join(str(x[1]) for x in keys)))
    return name, keys

def get_key_table(table):
    def transform(k, v):
        h, *t = k
        if not t:
            return {h: v}
        return {h: transform(t, v)}
    gencomp = (transform(k, v) for k, v in table.items())
    return deep_merge_dict(gencomp)

def go_up(num_lines=1):
    sayn('[A' * num_lines)
