# -*- coding: utf-8 -*-

import traceback
from sys import stdout, stderr

import pydash

from acatpy.speak import shell_quote, info_cmd_pref, info_pref, warn_pref, iwarn_pref
from acatpy.speak import debug_pref, error_pref, error_pref_only, ok_s, not_ok_s
from acatpy.colors import cyan, yellow, bright_red

class _Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream

    def write(self, s, **kw):
        self.stream.write(s, **kw)
        self.stream.flush()

    def __getattr__(self, *a, **kw):
        return getattr(self.stream, *a, **kw)


levels = {
    'ERROR': 0,
    'WARN': 1,
    'INFO': 2,
    'INFO_CMD': 3,
    'DEBUG': 4,
}
levels_reverse = {v: k for k, v in levels.items()}
stdout_u = _Unbuffered(stdout)
stderr_u = _Unbuffered(stderr)

state = {
    'level': levels['INFO_CMD'],
}

def set_level(level):
    state['level'] = levels[level]

def get_level():
    return levels_reverse[state['level']]

def _check_level(level):
    return state['level'] >= levels[level]

def unshift(xs):
    x = xs[0]
    xs[:] = xs[1:]
    return x

def info_cmd_str(s):
    return info_cmd_pref() + s

def info_cmdn(xs, shell=False):
    if not _check_level('INFO_CMD'): return
    if not xs: return
    if shell:
        s = ' '.join(xs)
    else:
        ys = [shell_quote(_) for _ in xs]
        [z, *zs] = ys
        s = ' '.join([cyan(z), *zs])
    stdout_u.write(info_cmd_str(s))

def info_cmd(s, **kw):
    info_cmdn(s, **kw)
    stdout_u.write('\n')

def info_cmd_liken(s):
    if not _check_level('INFO_CMD'): return
    if not s: return
    stdout_u.write(info_cmd_str(s))

def info_cmd_like(s, **kw):
    info_cmd_liken(s, **kw)
    stdout_u.write('\n')

def info_str(s):
    return info_pref() + s

def infon(s):
    if not _check_level('INFO'): return
    stdout_u.write(info_str(s))

def info(s):
    infon(s + '\n')

def debug_str(s):
    return debug_pref() + s

def debugn(s):
    if not _check_level('DEBUG'): return
    stdout_u.write(debug_str(s))

def debug(s):
    debugn(s + '\n')

def warn_str(s):
    return warn_pref() + s

def warnn(s="Something's wrong"):
    if not _check_level('WARN'): return
    stderr_u.write(warn_str(s))

def warn(s="Something's wrong"):
    warnn(str(s) + '\n')

def iwarn_str(s):
    return iwarn_pref() + s

def iwarnn(s="Something's wrong", rewind_stack=1):
    stack = traceback.extract_stack()
    frame = stack[-1 - rewind_stack]
    filename = frame.filename
    lineno = frame.lineno
    stack_s = '%s:%s' % (yellow(filename), bright_red(lineno))
    warning = '(%s) %s' % (stack_s, s)
    stderr_u.write(iwarn_str(warning))

def iwarn(s="Something's wrong"):
    iwarnn(str(s) + '\n', rewind_stack=2)

def ok():
    stdout_u.write('%s\n' % ok_s())

def not_ok():
    stdout_u.write('%s\n' % not_ok_s())

def sayn(s):
    stdout_u.write(s)

def say(s):
    sayn(s + '\n')

def only_whitespace(s):
    return s.strip() == ''

def err_str(s=''):
    if s and not only_whitespace(s):
        pref = error_pref()
    else:
        pref = error_pref_only()
    return pref + s

def get_stack_trace():
    trace = ''.join(_ for _ in traceback.format_stack())
    return 'Traceback (most recent call last):\n%s' % trace

def errorn(s=None):
    if not _check_level('ERROR'): return
    if s: stderr_u.write(err_str(s))
    else: stderr_u.write(err_str())
    """
    line = lambda l: '    %s %s %s %s(...)\n%s     %s' % (
        debug_pref(),
        bright_red(l.lineno),
        l.filename,
        bright_red(l.name),
        ' ' * len(strip_colors(debug_pref())),
        l.line,
    )
    stderr_u.write(get_stack_trace())
    """

def abridge_path(path):
    s = path.split('/')
    last = s.pop()
    s = [pydash.get(_, 0) or '' for _ in s]
    return '/'.join(s + [last])

def error(s=None):
    if s: errorn(s + '\n')
    else: errorn('\n')
