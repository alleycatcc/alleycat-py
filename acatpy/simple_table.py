# -*- coding: utf-8 -*-

from functools import namedtuple, partial
from sys import stdout

from acatpy.colors import underline, strip_colors

def fmt_header_default(x, use_colors=True):
    return underline(x.str) if use_colors else x.str

class Table(object):
    def __init__(self, fields=None, print_header=20, fmt_header=fmt_header_default, orient='horizontal', fh_output=stdout):
        self._fields = [x[0] for x in fields or []]
        self._widths = [x[1] for x in fields or []]
        self._widths = [max(x, len(y)) for x, y in zip(self._widths, self._fields)]
        self._num_fields = len(fields)
        self._print_header = print_header
        self._n = -1
        self._first = True
        use_colors = fh_output.isatty()
        self._fmt_header = partial(fmt_header, use_colors=use_colors)
        self._fh_output = fh_output
        if orient == 'horizontal': self.horizontal()
        elif orient == 'vertical': self.vertical()
        else: raise Exception('bad arg')

    def _print(self, x):
        self._fh_output.write(x)
        self._fh_output.write('\n')
        self._fh_output.flush()

    def _fmt_header_wrapper(self, n, s):
        F = namedtuple('F', ['idx', 'str'])
        return self._fmt_header(F(n, s))

    def _update_horizontal(self):
        self._n = (self._n + 1) % self._print_header
        if self._n != 0: return
        if self._first: self._first = False
        else: self._print('')
        fmt = self._fmt_header_wrapper
        xs = (fmt(n, pad(n, x)) for n, x in zip(self._widths, self._fields))
        self._print(' '.join(xs))

    def row(self, *vals):
        strs = (str(_) for _ in vals)
        return self._row(*strs)

    def _row_horizontal(self, *vals):
        if self._num_fields != len(vals): raise Exception('bad #')
        self._update_horizontal()
        zipper = zip(self._widths, vals)
        xs = (pad(n, str(val)) for n, val in zipper)
        self._print(' '.join(xs))

    def _row_vertical(self, *vals):
        if self._num_fields != len(vals): raise Exception('bad #')
        self._print('')
        fmt = self._fmt_header_wrapper
        n = 0
        for name, val in zip(self._fields, vals):
            n += 1
            self._print('%s: %s' % (fmt(n, name), val))

    def horizontal(self):
        self._n = -1
        self._first = True
        self._row = self._row_horizontal

    def vertical(self):
        self._row = self._row_vertical

def pad(length, the_str):
    # --- @todo necessary?
    stripped = strip_colors(the_str)
    ln = len(stripped)
    if ln >= length:
        return '%s…' % the_str[0:length - 1]
    m = ' ' * (length - ln)
    t = the_str + m
    return t
