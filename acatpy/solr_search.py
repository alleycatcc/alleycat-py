# -*- coding: utf-8 -*-

import pysolr

DEBUG_QUERY = False

class Search(object):
    def __init__(self, url):
        self._url = url
        self._solr = pysolr.Solr(url)

    def search(self, query, *args, **kw):
        try:
            rs = _do_search_go(self._solr, query, *args, **kw)
            return [], rs
        except pysolr.SolrError as e:
            return [e], None

def init(url):
    return Search(url)

def _do_search_go(solr, query, *args, **kw):
    if DEBUG_QUERY:
        print('\n\n\n\n')
        print('query %s' % query)
        print('\n\n\n\n')
    return solr.search(query, *args, **kw)
