# -*- coding: utf-8 -*-

from lxml import etree
from lxml.etree import XMLSyntaxError, XSLTParseError, XML, XSLT
from lxml.etree import fromstring as lxml_parse_string

from acatpy.io.speak import warn
from acatpy.io.io import cmd

# --- note, returns a string, while xslt_transform returns an xml tree.
def xslt_transform_saxon_java(xml, xslt_path, saxon_jar_path, java_bin='java', java_args=None, throw=True, unparse=False, encode=True, parse=True):
    java_args = java_args or []
    args = [
        java_bin,
        *java_args,
        '-cp', saxon_jar_path,
        'net.sf.saxon.Transform',
        '-s:-', '-xsl:%s' % xslt_path,
    ]

    if unparse:
        unparsed = etree.tostring(xml)
    elif encode:
        unparsed = xml.encode()
    else:
        unparsed = xml
    ret = cmd(args, input=unparsed, capture_stdout=True, die=False)
    err = None
    if not ret.ok:
        err = "Couldn't run java xslt transform: %s" % ret.errstr
        if throw:
            raise Exception(err)
        return err, None
    if parse:
        try:
            retval = etree.fromstring(ret.out)
        except XMLSyntaxError as e:
            err = "Couldn't parse xslt result: %s" % e
            warn(err)
            if throw:
                raise e
            return err, None
    else:
        retval = ret.out
    if throw:
        return retval
    return None, retval

def make_extensions_dict(extensions):
    return {(ns, func_name): cls() for ns, func_name, cls in extensions}

def xslt_transform(xml, xslt, parse_xml=False, extensions=[], quiet=False, throw=False):
    xslt_kw = {}
    xslt_root = XML(xslt)

    if parse_xml:
        try:
            xml_tree = etree.fromstring(xml.encode())
        except XMLSyntaxError as e:
            err = "Can't parse xml: %s" % e
            if not quiet: warn(err)
            if throw: raise e
            return err, None
    else:
        xml_tree = xml
    if extensions:
        xslt_kw['extensions'] = make_extensions_dict(extensions)
    err, exc = None, None
    transform = None
    try:
        transform = XSLT(xslt_root, **xslt_kw)
        result = transform(xml_tree)
    except XSLTParseError as e:
        exc = e
        err = "Can't parse xslt: %s" % exc
        if transform: err += ' %s' % transform.error_log
    except Exception as e:
        exc = e
        err = "Can't run xslt: %s" % exc
        if transform: err += ' %s' % transform.error_log
    if err:
        if not quiet: warn(err)
        if throw: raise exc
        return err, None
    if throw:
        return result
    else:
        return None, result

def get_text(node):
    return (node.text or '') + ''.join(
        (get_text(_) or '') + (_.tail or '') for _ in node
    )

def parse_xml(xml, throw=False, quiet=False):
    try:
        parsed = lxml_parse_string(xml.encode())
    except XMLSyntaxError as e:
        err = "Could not parse: err=%s, xml=%s" % (e, xml)
        if not quiet:
            warn(err)
        if throw:
            raise e
        return err, None
    if parsed is not None:
        if throw:
            return parsed
        else:
            return None, parsed
