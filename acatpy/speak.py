# -*- coding: utf-8 -*-

import re

from acatpy.colors import green, blue, bright_red, yellow, red

info_pref = lambda: blue('٭ ')
info_cmd_pref = lambda: green('٭ ')
warn_pref = lambda: '%s Warning: ' % bright_red('٭')
iwarn_pref = lambda: '%s Internal warning: ' % bright_red('٭')
debug_pref = lambda: yellow('٭') + blue('٭') + yellow('٭ ')
error_pref = lambda: '%s Error: ' % red('٭')
error_pref_only = lambda: '%s Error.' % red('٭')

def shell_quote(x):
    chars = [
        r'\]',
        '[', r'\s', ';', '$', '!',
        '&', '*', '(', ')', '{', '}',
        '<', '>', '?', '~', '`', "'", '"',
        '|', '#', '%', '^', '~', ';',
        r'\\',
    ]
    regex = r'.*[%s]' % ''.join(chars)
    if not re.match(regex, x): return x
    y = re.sub("'", r"'\''", x)
    return "'%s'" % y

def ok_s():
    return green('✔')

def not_ok_s():
    return red('✘')
