# -*- coding: utf-8 -*-

import traceback
from functools import namedtuple

from acatpy.colors import yellow, bright_red
from acatpy.io.speak import debug_pref

# --- Fout is useful for an error-passing style, e.g. in library code when
# you don't want to throw an exception. As you unwind the stack you can pass
# the 'Fout' down, optionally annotating it at various points. The result
# will resemble a stack trace but consist only of frames you annotated.

# Example: `a()` calls `b()` which calls `c()`, and they all return a tuple
# `(err, results)`:

#     def c():
#        ...
#        return Fout('Problem with c ...'), None
#
#     def b():
#        err, results = c()
#        if err:
#            err.add('Problem with b ...')
#            return err, None
#        return None, results
#
#     def a():
#        err, results = b()
#        if err:
#            err.add('Problem with a ...')
#            return err, None
#        return None, results
#
#     err, _ = a()
#     print(err)

# --- results in:
#
#     ٭٭٭ test.py:52 c() → Problem with c ...
#     ٭٭٭ test.py:57 b() → Problem with b ...
#     ٭٭٭ test.py:64 a() → Problem with a ...

class Fout(object):
    Info = namedtuple('Info', ['message', 'func_name', 'filename', 'lineno'])

    def stack_frame(stack, rewind=0):
        length = len(stack)
        if length < rewind + 1:
            raise Exception("Can't rewind stack by %s (stack=%s)" % (rewind, stack))
        frame = stack[-1 - rewind]
        return frame.name, frame.filename, frame.lineno

    def __init__(self, *errs):
        self._errs = []
        if errs: self.add(*errs, _stack_rewind=2)

    def add(self, *errs, _stack_rewind=1):
        def fmt(err):
            if isinstance(err, Exception):
                return ' '.join(err.args)
            if hasattr(err, 'decode'):
                return err.decode()
            else:
                return err

        es = ': '.join(fmt(_) for _ in errs)
        func_name, _filename, lineno = Fout.stack_frame(
            traceback.extract_stack(), rewind=_stack_rewind,
        )
        filename = (_filename or '').split('/')[-1]
        err = Fout.Info(es, func_name, filename, lineno)
        self._errs.append(err)

    def __repr__(self):
        line = lambda frame: '    %s%s:%s %s() → %s' % (
            debug_pref(),
            frame.filename,
            yellow(frame.lineno),
            (bright_red(frame.func_name)),
            frame.message,
        )
        return '\n' + '\n'.join(line(_) for _ in self._errs)
