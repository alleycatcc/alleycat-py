# -*- coding: utf-8 -*-

from acatpy.colors import bright_red, yellow, underline, cyan, magenta
from acatpy.http import http_request_json_path_match, http_request_json, http_request_xml
from acatpy.io.io import die, cmd
from acatpy.io.speak import info
from acatpy.speak import ok_s as speak_ok_s, not_ok_s as speak_not_ok_s
from acatpy.util import imerge

_die = die

def has_core(host, port, core_name):
    # --- also possible to add core=x to the params; like this it will query
    # them all.
    uri = '/solr/admin/cores?action=STATUS'
    obj_path = ['status', core_name, 'uptime']
    has, *_ = http_request_json_path_match(
        obj_path, lambda x: x > 0,
        host=host, port=port, uri=uri,
        warn_connection_error=False)
    info_util('core %s exists %s' % (yellow(core_name), speak_ok_s() if has else speak_not_ok_s()))
    return bool(has)

# --- local only (uses the 'assert' command, with the given url)
def is_running(solrcmd, url):
    info_util('check solr running on %s' % yellow(url))
    ret = solr_cmd(solrcmd, [
        'assert', '-s', url,
    ], die=False, complain=False, ignore_stderr=True)
    ok_str = speak_ok_s() if ret.ok else speak_not_ok_s()
    info_util('solr running: %s' % ok_str)
    return ret.ok

# --- local only.
def config_no_data_driven(solrcmd, port, core_name):
    info_util('set core %s to non-data-driven mode' % yellow(core_name))
    return solr_cmd(solrcmd, [
        'config', '-c', core_name,
        '-p', port,
        '-action', 'set-user-property',
        '-property', 'update.autoCreateFields',
        '-value', 'false',
    ])

def add_field(host, port, core_name, field_spec):
    name = field_spec['name']
    info_util('add field %s' % cyan(name))
    data = {'add-field': field_spec}
    uri = '/solr/%s/schema' % core_name
    ret = http_request_json(host=host, port=port, uri=uri, json=data)
    if not ret.ok:
        die('Failed to add field %s: %s' % (bright_red(name), ret.req.content))

def update_request(host, port, core_name, xml, die=True):
    uri = '/solr/%s/update' % core_name
    ret = http_request_xml(host=host, port=port, uri=uri, xml=xml)
    if not ret.ok:
        if die:
            _die('ret.req.content: %s' % ret.req.content)
        err = 'status: %s, content: %s' % (ret.req.status_code, ret.req.content.decode() or 'none')
        return err, ret
    return None, ret

def add_copy_field(host, port, core_name, src, dest):
    info_util('add copy field %s -> %s' % (cyan(src), magenta(dest)))
    data = {'add-copy-field': {'source': src, 'dest': dest}}
    uri = '/solr/%s/schema' % core_name
    ret = http_request_json(host=host, port=port, uri=uri, json=data)
    if not ret.ok:
        die('Failed to add copy field %s: %s' % (bright_red(src), ret.req.content))

def solr_cmd(solrcmd, args, ignore_stdout=False, ignore_stderr=False, **kw):
    ourkw = imerge({
        'die': True,
        'capture_stderr': True,
        'capture_stdout': True}, kw)
    ret = cmd([
        solrcmd, *args
    ], **ourkw)

    def prnt(s, x):
        if x: info_util('%s: \n%s\n' % (s, '\n'.join(['    %s' % _ for _ in x.splitlines()])))
    if not ignore_stdout: prnt('cmd output', ret.out)
    if not ignore_stderr: prnt('cmd error output', ret.err)
    return ret

def info_util(s):
    info('%s: %s' % (underline('solr-util'), s))
