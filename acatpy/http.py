# -*- coding: utf-8 -*-

from json import JSONDecodeError

import requests
import pydash

from acatpy.io.io import die
from acatpy.io.speak import warn, info_cmd_like
from acatpy.colors import bright_red, bright_blue

class Generic(object):
    def __init__(self, **kw):
        [setattr(self, k, v) for k, v in kw.items()]


def _http_request(
        host=None, port=None, uri=None, protocol=None, method=None,
        headers={},
        data=None, json=None,
        warn_connection_error=True, warn_invalid_response=True,
        warn_bad_status=True):
    url = '%s://%s:%s%s' % (protocol, host, port, uri)
    call = getattr(requests, method)
    callkw = {}
    if data: callkw = {'data': data}
    elif json: callkw = {'json': json}
    # --- ignore json if data is present; data is for forms; json should be
    # the data structure, not json encoded.
    info_cmd_like('%s → %s' % (bright_blue('%s json' % method), url))
    try:
        req = call(url, headers=headers, **callkw)
    except ConnectionError:
        if warn_connection_error:
            warn('Connection refused to %s' % bright_red(url))
        return None
    code = req.status_code
    if code != 200 and warn_bad_status:
        warn('HTTP(S) call returned %s' % bright_red(str(code)))
    return req

def http_request_json_path_match(
        obj_path, pred,
        host='localhost', port=80, uri='/', protocol='http',
        status_ok=lambda x: x == 200,
        warn_connection_error=True, warn_invalid_response=True,
        warn_bad_status=True):
    req = _http_request(
        host=host, port=port, uri=uri, protocol=protocol, method='get',
        warn_connection_error=warn_connection_error,
        warn_invalid_response=warn_invalid_response,
        warn_bad_status=warn_bad_status)
    if req is None: return None, Generic(ok=False)
    code = req.status_code
    # --- ok means request was sent, status passed, and response was valid
    # json.
    try:
        data = req.json()
    except JSONDecodeError:
        if warn_invalid_response:
            warn('HTTP(S) call returned invalid json: %s' % req.content)
        return None, Generic(ok=False, req=req)
    val = pydash.get(data, obj_path)
    ok = status_ok(code)
    if val is None: return False, Generic(ok=ok, req=req)
    return pred(val), Generic(ok=ok, req=req)

def _http_request_data(
        host='localhost', port=80, uri='/', protocol='http', method='post',
        headers={},
        data=None, json=None,
        status_ok=lambda x: x == 200,
        warn_connection_error=True, warn_invalid_response=True,
        warn_bad_status=True):
    req = _http_request(
        host=host, port=port, uri=uri, protocol=protocol, method=method,
        headers=headers, json=json, data=data,
        warn_connection_error=warn_connection_error,
        warn_invalid_response=warn_invalid_response,
        warn_bad_status=warn_bad_status)
    if req is None: return Generic(ok=False)
    code = req.status_code
    return Generic(ok=status_ok(code), req=req)

def http_request_json(*args, json=None, **kw):
    if json is None: die('need %s parameter' % bright_red('json'))
    return _http_request_data(*args, json=json, **kw)

def http_request_data(*args, data=None, **kw):
    if data is None: die('need %s parameter' % bright_red('data'))
    return _http_request_data(*args, data=data, **kw)

def http_request_xml(*args, xml=None, headers={}, **kw):
    if xml is None: die('need %s parameter' % bright_red('xml'))
    headers['Content-Type'] = 'text/xml'
    return _http_request_data(*args, headers=headers, data=xml, **kw)
