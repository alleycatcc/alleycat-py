# -*- coding: utf-8 -*-

import os.path as path
from functools import reduce
from re import sub as re_sub
from uuid import uuid4

import pydash

ident = lambda x: x
uniq_unordered = lambda xs: list(set(xs))

def pathjoin(*paths):
    return path.realpath(path.join(*paths))

def pathsibling(a, b):
    return pathjoin(a, '..', b)

def pathparent(a):
    return pathjoin(a, '..')

def is_string(x):
    return isinstance(x, str)

def escape_string(x):
    if not is_string(x):
        return x
    y = re_sub("'", "\\'", x)
    return "'%s'" % y

# --- return shallow immutable merge of two dictionaries.
def imerge(a, b):
    return {**a.copy(), **b}

# --- like `filter`, but returns the number rejected, and the result.
def filterc(pred, xs):
    def reducer(acc, x):
        num, lines = acc
        if not pred(x): num += 1
        else: lines.append(x)
        return (num, lines)
    return reduce(reducer, xs, (0, []))

# --- like `reject`, but returns the number rejected, and the result.
def rejectc(pred, xs):
    return filterc(lambda x: not pred(x), xs)

def slurp(filename):
    return open(filename, 'r').read()

def slurpl(filename):
    return open(filename, 'r').readlines()

def make_uuid():
    return uuid4().hex

# --- check `x` against every predicate in `preds` for truthiness.
# --- return `False` if any of them doesn't match, otherwise `True`.
def against_all(preds, x):
    return pydash.find(preds, lambda p: not p(x)) is None

def comma(_, x):
    return x

def noop(*args, **kw):
    pass

def find_pred(collection, pred):
    for c in collection:
        p = pred(c)
        if p: return p

def intersect_with(xs, ys, f, g, cmp):
    p = lambda x, y: cmp(x, y)
    return [(x, y) for x in xs for y in ys if p(f(x), g(y))]

def deep_merge_dict(ds):
    def update(d, u):
        for k, v in u.items():
            if isinstance(v, dict):
                r = update(d.get(k, {}), v)
                d[k] = r
            else:
                d[k] = u[k]
        return d

    ret = {}
    for d in ds:
        update(ret, d)
    return ret

def default(x, d):
    return d if x is None else x

def default_with(f, d, x):
    return d if x is None else f(x)

def subn(pattern, repl, string, **kw):
    num_replaced = 0

    def replace(m):
        nonlocal num_replaced
        num_replaced += 1
        return repl
    new = re_sub(pattern, replace, string, **kw)
    return num_replaced, new


'''
Example: you want to provide a keyword arg (e.g. `rm`) to your function, but
the arg has the same name as an existing symbol (e.g. a function named
`rm`), and you don't want to shadow the existing symbol. Add this decorator
to be able to call the function using the original name for the keyword arg
(`rm`) but to be able to access it from within the function using a
different name (e.g. `rmarg`).

from tools import rm

@alias_kw(rmarg='rm')
def init(..., rmarg=True):
    if rmarg:
        rm()

def foo():
    init(rm=False)
'''

def alias_kw(**kw_alias):
    def decorator(orig):
        def f(*args, **kw_h):
            for alias_from, alias_to in kw_alias.items():
                if alias_to in kw_h:
                    kw_h[alias_from] = kw_h.pop(alias_to)
            return orig(*args, **kw_h)
        return f
    return decorator
